//
//

#ifndef Mass_h
#define Mass_h

#include <stdio.h>

#include "SceneObject.h"

class Mass : public SceneObject
{
public:
  bool staticObject = false;
  
  float mass = 1.0;
  
  vec3 force = vec3(0.0, 0.0, 0.0);
  vec3 velocity = vec3(0.0, 0.0, 0.0);
  vec3 position = vec3(0.0, 0.0, 0.0);
  
  Mass();
  
  void updateVelocityAndPosition(float dt);
};

#endif /* Mass_h */

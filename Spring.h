//
//

#ifndef Spring_h
#define Spring_h

#include <stdio.h>

#include "SceneObject.h"
#include "Mass.h"


class Spring : public SceneObject
{
  
public:
  Mass* a;
  Mass* b;

  float springConstant;
  float restLength;
  
  float criticalDamping;
  float lambda = 1.0;
  
  Spring(Mass* A, Mass* B, float k, float d);
  
  void applyForceToMasses();
  void updateFromMasses();
};


#endif /* Spring_h */

//
//

#include "MassSpringScene.h"

MassSpringScene::MassSpringScene()
{
  Mass* a = new Mass();
  a->position = vec3(0.0, 2.0, 0.0);
  a->staticObject = true;
  
  Mass* b = new Mass();
  b->position = vec3(0.1, 1.5, 0.0);
  
  Spring* s = new Spring(a, b, 10.0, 2.5);
  s->lambda = 0.05;
  
  airDamp = 0.0;
  
  addMass(a);
  addMass(b);
  addSpring(s);
}



void MassSpringScene::updateScene(float dt)
{
  float increment = dt / (float) numIterations;
  
  for (int i = 0; i < numIterations; ++i) {
    time += increment;
    // Use the springs to apply forces to the masses
    for (Spring* spring : springs)
      spring->applyForceToMasses();
    
    // Update the velocity and positions of the masses
    for (Mass* mass : masses) {
      // Apply air damping and gravity
      mass->force -= airDamp * mass->velocity;
      mass->force -= gravity * mass->mass;
      
      // Apply wind
      if (wind) {
        float force = windForce + sin(time / 10.0) * windForce * 0.6;
        
        #ifdef _LINUX_BUILD
          float x = windDirection.x + sin((random() % 360) / 180.0 * M_PI) * windForce;
          float y = windDirection.y + sin((random() % 360) / 180.0 * M_PI) * windForce;
          float z = windDirection.z + sin((random() % 360) / 180.0 * M_PI) * windForce;
        #else
          float x = windDirection.x + sin((arc4random() % 360) / 180.0 * M_PI) * windForce;
          float y = windDirection.y + sin((arc4random() % 360) / 180.0 * M_PI) * windForce;
          float z = windDirection.z + sin((arc4random() % 360) / 180.0 * M_PI) * windForce;
        #endif

        vec3 direction = vec3(x,y,z);
        normalize(direction);
        
        mass->force += force * direction;
      }
      
      // Have a ground plane at y = -1
      if (groundPlane && mass->position.y < -1.0) {
        float x = -1.0 - mass->position.y;
        mass->force += 5000.0f * x * vec3(0.0, 1.0, 0.0);
      }
      
      mass->updateVelocityAndPosition(increment);
    }
  }
  
  // Update the visuals of the and masses
  for (Mass* mass : masses)
    mass->setLocalPosition(mass->position);
  
  for (Spring* spring : springs)
    spring->updateFromMasses();
}


void MassSpringScene::addMass(Mass* mass)
{
  masses.push_back(mass);
  children.push_back(mass);
}


void MassSpringScene::addSpring(Spring* spring)
{
  springs.push_back(spring);
  children.push_back(spring);
}


void MassSpringScene::clearScene()
{
  masses.clear();
  children.clear();
}


/* Scene Creation Functions
 *
 */

MassSpringScene* MassSpringScene::createStiffChain(int length)
{
  MassSpringScene* chain = new MassSpringScene();
  chain->clearScene();
  
  Spring* spring;
  Mass* mass = new Mass();
  mass->position = vec3(-0.5, 2.0, 0.0);
  mass->staticObject = true;
  
  chain->addMass(mass);
  
  for (int i = 0; i < length; ++i) {
    mass = new Mass();
    mass->position = vec3(-0.5 + 0.2 * i, 2.0, 0.0);
    chain->addMass(mass);
    
    spring = new Spring(chain->masses[i], chain->masses[i+1], 10000.0, 0.2);
    chain->addSpring(spring);
  }
  
  return chain;
}


MassSpringScene* MassSpringScene::createJellyCube(int width)
{
  MassSpringScene* jelly = new MassSpringScene();
  jelly->clearScene();
  
  jelly->groundPlane = true;
  
  float initial = -0.2 * (float) width / 2.0;
  
  // Add the masses
  for (int i = 0; i < width; ++i) {
    for (int j = 0; j < width; ++j) {
      for (int k = 0; k < width; ++k) {

        #ifdef _LINUX_BUILD
          float x = sin(random()) / 1000.0;
          float y = sin(random()) / 1000.0;
          float z = sin(random()) / 1000.0;
        #else
          float x = sin(arc4random()) / 1000.0;
          float y = sin(arc4random()) / 1000.0;
          float z = sin(arc4random()) / 1000.0;
        #endif

        Mass* mass = new Mass();
        mass->position = vec3(initial + x, 1.0 + y, initial + z) + 0.2f * vec3(i, j, k);
        mass->mass = 0.025;
        
        jelly->addMass(mass);
      }
    }
  }
  
  jelly->airDamp = 0.01;
  
  const float springConstant = 35.0 + width * 4.0;
  const float lamda = 0.025;

  const int iInc = width * width;
  const int jInc = width;
  const int kInc = 1;
  
  
  // Add the springs
  for (int i = 0; i < width; ++i) {
    for (int j = 0; j < width; ++j) {
      for (int k = 0; k < width; ++k) {
        
        int index = i * iInc + j * jInc + k * kInc;
        
        Mass* a = jelly->masses[index];
        Mass* b;
        float dist;
        Spring* spring;
        
        if (i < width - 1) {
          b = jelly->masses[index + iInc];
          dist = length(a->position - b->position);
          
          spring = new Spring(a, b, springConstant, dist);
          spring->lambda = lamda;
          jelly->addSpring(spring);
          
          if (i < width - 2) {
            b = jelly->masses[index + 2 * iInc];
            dist = length(a->position - b->position);
            
            spring = new Spring(a, b, 2.0 * springConstant, dist);
            spring->lambda = lamda;
            jelly->addSpring(spring);
          }
          
          
          if (j < width - 1) {
            b = jelly->masses[index + iInc + jInc];
            dist = length(a->position - b->position);
            
            spring = new Spring(a, b, springConstant, dist);
            spring->lambda = lamda;
            jelly->addSpring(spring);
          }
          
          if (j > 0) {
            b = jelly->masses[index + iInc - jInc];
            dist = length(a->position - b->position);
            
            spring = new Spring(a, b, springConstant, dist);
            spring->lambda = lamda;
            jelly->addSpring(spring);
          }
          
          if (k < width - 1) {
            b = jelly->masses[index + iInc + kInc];
            dist = length(a->position - b->position);
            
            spring = new Spring(a, b, springConstant, dist);
            spring->lambda = lamda;
            jelly->addSpring(spring);
          }
          
          if (k > 0) {
            b = jelly->masses[index + iInc - kInc];
            dist = length(a->position - b->position);
            
            spring = new Spring(a, b, springConstant, dist);
            spring->lambda = lamda;
            jelly->addSpring(spring);
          }
          
          //
          if (j < width - 1 && k < width - 1) {
            b = jelly->masses[index + iInc + jInc + kInc];
            dist = length(a->position - b->position);
            
            spring = new Spring(a, b, springConstant, dist);
            spring->lambda = lamda;
            jelly->addSpring(spring);
          }
          
          //
          if (j > 0 && k > 0) {
            b = jelly->masses[index + iInc - jInc - kInc];
            dist = length(a->position - b->position);
            
            spring = new Spring(a, b, springConstant, dist);
            spring->lambda = lamda;
            jelly->addSpring(spring);
          }

        }
        
        if (j < width - 1) {
          b = jelly->masses[index + jInc];
          float dist = length(a->position - b->position);
          
          spring = new Spring(a, b, springConstant, dist);
          spring->lambda = lamda;
          jelly->addSpring(spring);
          
          if (j < width - 2) {
            b = jelly->masses[index + 2 * jInc];
            dist = length(a->position - b->position);
            
            spring = new Spring(a, b, 2.0 * springConstant, dist);
            spring->lambda = lamda;
            jelly->addSpring(spring);
          }

          
          if (k < width - 1) {
            b = jelly->masses[index + jInc + kInc];
            dist = length(a->position - b->position);
            
            spring = new Spring(a, b, springConstant, dist);
            spring->lambda = lamda;
            jelly->addSpring(spring);
          }
          
          if (k > 0) {
            b = jelly->masses[index + jInc - kInc];
            dist = length(a->position - b->position);
            
            spring = new Spring(a, b, springConstant, dist);
            spring->lambda = lamda;
            jelly->addSpring(spring);
          }

          //
          if (i > 0 && k > 0) {
            b = jelly->masses[index - iInc + jInc - kInc];
            dist = length(a->position - b->position);
            
            spring = new Spring(a, b, springConstant, dist);
            spring->lambda = lamda;
            jelly->addSpring(spring);
          }

        }
        
        if (k < width - 1) {
          b = jelly->masses[index + kInc];
          dist = length(a->position - b->position);
          
          spring = new Spring(a, b, springConstant, dist);
          spring->lambda = lamda;
          jelly->addSpring(spring);
          
          if (k < width - 2) {
            b = jelly->masses[index + 2 * kInc];
            dist = length(a->position - b->position);
            
            spring = new Spring(a, b, 2.0 * springConstant, dist);
            spring->lambda = lamda;
            jelly->addSpring(spring);
          }
          
          //
          if (i > 0 && j > 0) {
            b = jelly->masses[index - iInc - jInc + kInc];
            dist = length(a->position - b->position);
            
            spring = new Spring(a, b, springConstant, dist);
            spring->lambda = lamda;
            jelly->addSpring(spring);
          }
        }
      }
    }
  }

  return jelly;
}


MassSpringScene* MassSpringScene::createCloth(int width, int height)
{
  MassSpringScene* cloth = new MassSpringScene();
  cloth->clearScene();
  
  cloth->airDamp = 0.075;

  
  float initial = -0.1 * (float)width / 2.0;

  // Add the masses
  for (int i = 0; i < width; ++i) {
    for (int j = 0; j < height; ++j) {

      #ifdef _LINUX_BUILD
        float theta = (random() % 360) / 180.0 * M_PI;
      #else
        float theta = (arc4random() % 360) / 180.0 * M_PI;
      #endif
      
      Mass* mass = new Mass();
      mass->position = vec3(initial + 0.1 * i, 1.5 - 0.01 * j, sin(theta) * 0.25);
      mass->mass = 0.025;
      
      if ((i % 5 == 0 || i == width - 1) && j == 0)
        mass->staticObject = true;
      
      cloth->addMass(mass);
    }
  }
  
  // Add the Springs
  for (int i = 0; i < width; ++i) {
    for (int j = 0; j < height; ++j) {
      Mass* a = cloth->masses[i * height + j];

      if (j < height - 1) {
        Mass* b = cloth->masses[i * height + (j + 1)];
        Spring* spring = new Spring(a, b, 750, 0.1);
        cloth->addSpring(spring);
      }
   
      if (i < width - 1) {
        Mass* c = cloth->masses[(i + 1) * height + j];
        Spring* spring = new Spring(a, c, 750, 0.1);
        cloth->addSpring(spring);
      }
    }
  }
  
  return cloth;
}


MassSpringScene* MassSpringScene::createFlag(int width, int height)
{
  MassSpringScene* cloth = new MassSpringScene();
  cloth->clearScene();
  
  cloth->airDamp = 0.075;

  
  cloth->wind = true;
  
  cloth->windDirection = vec3(1.0, 0.4, 0.0);
  normalize(cloth->windDirection);
  
  cloth->windForce = 0.4;
  
  // Add the masses
  for (int i = 0; i < width; ++i) {
    for (int j = 0; j < height; ++j) {
      Mass* mass = new Mass();
      mass->position = vec3(-1.0 + 0.1 * i, 1.5 - 0.1 * j, 0.0);
      mass->mass = 0.025;
      
      if ((j % 5 == 0 || j == width - 1) && i == 0)
        mass->staticObject = true;
      
      cloth->addMass(mass);
    }
  }
  
  float springConstant = 500.0;
  float dist = 0.1;
  
  // Add the Springs
  for (int i = 0; i < width; ++i) {
    for (int j = 0; j < height; ++j) {
      Mass* a = cloth->masses[i * height + j];
      
      if (j < height - 1) {
        Mass* b = cloth->masses[i * height + (j + 1)];
        Spring* spring = new Spring(a, b, springConstant, dist);
        cloth->addSpring(spring);
      }
      
      if (i < width - 1) {
        Mass* c = cloth->masses[(i + 1) * height + j];
        Spring* spring = new Spring(a, c, springConstant, dist);
        cloth->addSpring(spring);
      }
    }
  }
  
  return cloth;
}


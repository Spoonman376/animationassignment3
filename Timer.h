//
//

#ifndef Timer_h
#define Timer_h

#include <stdio.h>
#include <chrono>
#include <ctime>

class Timer
{
public:
  void start();
  void stop();
  
  float elapsedMilliseconds();
  float elapsedSeconds();
  
  bool m_bRunning = false;
  
private:
  std::chrono::time_point<std::chrono::system_clock> m_StartTime;
  std::chrono::time_point<std::chrono::system_clock> m_EndTime;
};



#endif /* Timer_h */

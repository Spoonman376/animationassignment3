//
//

#ifndef Shader_h
#define Shader_h

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <iterator>
#include <algorithm>

#include "Utilities.h"

#include "Camera.h"

using namespace std;

class Shader
{
public:

  Shader();
  ~Shader();
  
  // OpenGL names for vertex and fragment shaders, shader program
  GLuint program;
  GLuint vertex;
  GLuint fragment;
  
  GLint viewLoc;
  GLint projLoc;
  GLint modelLoc;
  
  GLint scaleLoc;

  void updateView(mat4);
  void updateProj(mat4);
  void updateModel(mat4);
  void updateScale(vec3);
};


#endif /* Shader_h */


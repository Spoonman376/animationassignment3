//
//

#ifndef Utilities_h
#define Utilities_h

#include <iostream>
#include <fstream>
#include <string>
#include <iterator>
#include <algorithm>
#include <vector>


#ifdef _LINUX_BUILD
// Include glad
#include "glad/glad.h"

#else
// MAC
#include <OpenGL/gl3.h>

#endif

#include <glm.hpp>

using namespace std;
using namespace glm;



mat3 setAxisAngleRotationRad(const vec3& a_axis,
                             const float& a_angleRad);

#endif /* Utilities_h */

//
//

#ifndef SceneObject_h
#define SceneObject_h

#include <stdio.h>
#include "Utilities.h"

#include "Shader.h"

class SceneObject
{
private:

  static const GLuint VERTEX_INDEX = 0;
  static const GLuint COLOUR_INDEX = 1;

  // OpenGL names for array buffer objects, vertex array object
  GLuint vertexBuffer;
  GLuint colourBuffer;
  GLuint vertexArray;
  GLsizei vertexCount;

protected:
  
  vector<GLfloat> vertices;
  vector<GLfloat> colours;
  
  void initializeBuffers();
  
  Shader *shader;
  
  void pushLine(vec3 a, vec3 b, vec3 colourA = vec3(0.0, 1.0, 0.0),  vec3 colourB = vec3(0.0, 1.0, 0.0));
  
  void pushTriangle(vec3 a, vec3 b, vec3 c, vec3 colourA = vec3(0.0, 1.0, 0.0),  vec3 colourB = vec3(0.0, 1.0, 0.0), vec3 colourC = vec3(0.0, 1.0, 0.0));
  
  vec3 scale = vec3(1,1,1);

  vec3 localPosition = vec3(0,0,0);
  mat3 localRotation = mat3(1);

  mat4 localTransform = mat4(1);

  bool parentRotation = true;
  bool ensureValidTransform = true;

public:
  bool isRendered = true;

  SceneObject();
  ~SceneObject();

  GLenum winding = GL_FRONT_AND_BACK;
  GLenum primativeType = GL_LINE_STRIP;

  vector<SceneObject *> children;

  vec3 getLocalPosition();
  mat3 getLocalRotation();
  mat4 getLocalTransform();

  void setLocalPosition(vec3 newPos);
  void setLocalRotation(mat3 newRot);
  void setLocalTransform(mat4 newTrans);
  
  vec3 getScale();
  void setScale(vec3 newScale);

  void updateVertices(const vector<GLfloat> &newVertices,const vector<GLfloat> &newColours);
  void updateVertices();

  void render(Shader* shader, mat4 parentTransform = mat4(1));
  
  static void createCube(SceneObject* cube, vec3 pos = vec3(0,0,0), float width = 1.0);
  static void createCylinder(SceneObject* cylinder, float height = 1.0, float radius = 1.0, vec3 pos = vec3(0,0,0), mat3 rot = mat3());

};

#endif /* SceneObject_h */


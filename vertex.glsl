#version 410

// location indices for these attributes correspond to those specified in the
// InitializeGeometry() function of the main program
layout(location = 0) in vec3 VertexPosition;
layout(location = 1) in vec3 VertexColour;

uniform mat4 view;
uniform mat4 model;
uniform mat4 proj;

uniform vec3 scale;

// output to be interpolated between vertices and passed to the fragment stage
out vec3 Colour;

void main()
{
  vec3 scaledPos = vec3(VertexPosition.x * scale.x, VertexPosition.y * scale.y, VertexPosition.z * scale.z);
  
  vec4 position = model * vec4(scaledPos, 1.0);
  gl_Position = proj * view * position;

  // assign output colour to be interpolated
  Colour = VertexColour;
}

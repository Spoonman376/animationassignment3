//
//

#include "SceneObject.h"

SceneObject::SceneObject()
{
  initializeBuffers();
}

SceneObject::~SceneObject()
{
  for (SceneObject* child : children)
    delete child;
}


void SceneObject::pushLine(vec3 a, vec3 b, vec3 colourA, vec3 colourB)
{
  vertices.push_back(a.x);
  vertices.push_back(a.y);
  vertices.push_back(a.z);
  
  colours.push_back(colourA.x);
  colours.push_back(colourA.y);
  colours.push_back(colourA.z);
  
  vertices.push_back(b.x);
  vertices.push_back(b.y);
  vertices.push_back(b.z);
  
  colours.push_back(colourB.x);
  colours.push_back(colourB.y);
  colours.push_back(colourB.z);  
}

void SceneObject::pushTriangle(vec3 a, vec3 b, vec3 c, vec3 colourA, vec3 colourB, vec3 colourC)
{
  vertices.push_back(a.x);
  vertices.push_back(a.y);
  vertices.push_back(a.z);
  
  colours.push_back(colourA.x);
  colours.push_back(colourA.y);
  colours.push_back(colourA.z);

  vertices.push_back(b.x);
  vertices.push_back(b.y);
  vertices.push_back(b.z);
  
  colours.push_back(colourB.x);
  colours.push_back(colourB.y);
  colours.push_back(colourB.z);

  vertices.push_back(c.x);
  vertices.push_back(c.y);
  vertices.push_back(c.z);
  
  colours.push_back(colourC.x);
  colours.push_back(colourC.y);
  colours.push_back(colourC.z);
}


vec3 SceneObject::getLocalPosition()
{
  return localPosition;
}

mat3 SceneObject::getLocalRotation()
{
  return localRotation;
}

mat4 SceneObject::getLocalTransform()
{
  return localTransform;
}

void SceneObject::setLocalPosition(vec3 newPos)
{
  localPosition = newPos;
  localTransform[3] = vec4(newPos, 1);
}

void SceneObject::setLocalRotation(mat3 newRot)
{
  localRotation = newRot;
  localTransform[0] = vec4(newRot[0],0);
  localTransform[1] = vec4(newRot[1],0);
  localTransform[2] = vec4(newRot[2],0);
}

void SceneObject::setLocalTransform(mat4 newTrans)
{
  localTransform = newTrans;
  localPosition = vec3(newTrans[3]);
  localRotation[0] = vec3(newTrans[0]);
  localRotation[1] = vec3(newTrans[1]);
  localRotation[2] = vec3(newTrans[2]);

  if (ensureValidTransform) {
    localTransform[0][3] = 0.0;
    localTransform[1][3] = 0.0;
    localTransform[2][3] = 0.0;
    localTransform[3][3] = 1.0;
  }
}


vec3 SceneObject::getScale()
{
  return scale;
}


void SceneObject::setScale(vec3 newScale)
{
  scale = newScale;
}



void SceneObject::render(Shader* shader, mat4 parentTransform) {
  if (!isRendered)
    return;

  if (!parentRotation) {
    parentTransform[0] = vec4(1,0,0,0);
    parentTransform[1] = vec4(0,1,0,0);
    parentTransform[2] = vec4(0,0,1,0);
  }

  shader->updateModel(parentTransform * localTransform);
  shader->updateScale(scale);

  // bind our shader program and the vertex array object containing our
  // scene geometry, then tell OpenGL to draw our geometry
  glUseProgram(shader->program);

  //buffer vertex data
  glBindVertexArray(vertexArray);

  // Change the winding
  glFrontFace(winding);

  //draw and clear
  glDrawArrays(primativeType, 0, vertexCount);

  // reset state to default (no shader or geometry bound)
  glBindVertexArray(0);
  glUseProgram(0);

  for (int i = 0; i < children.size(); ++i) {
    children[i]->render(shader, parentTransform * localTransform);
  }
}


void SceneObject::updateVertices(const vector<GLfloat> &newVertices,const vector<GLfloat> &newColours)
{
  assert(newVertices.size() % 3 == 0 && newVertices.size() / 3 == newColours.size() / 3);

  vertices = newVertices;
  colours = newColours;

  updateVertices();
}

void SceneObject::updateVertices()
{
  vertexCount = (GLsizei)vertices.size() / 3;
  
  glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
  glBufferData(GL_ARRAY_BUFFER, vertexCount * 3 * sizeof(float), &vertices[0], GL_STATIC_DRAW);

  glBindBuffer(GL_ARRAY_BUFFER, colourBuffer);
  glBufferData(GL_ARRAY_BUFFER, vertexCount * 3 * sizeof(float), &colours[0], GL_STATIC_DRAW);
}

void SceneObject::initializeBuffers()
{
  // verts is a array of floats that will have an even number of elements
  vertexCount = (GLsizei)vertices.size() / 3;
  
  // create an array buffer object for storing our vertices
  glGenBuffers(1, &vertexBuffer);
  glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
  glBufferData(GL_ARRAY_BUFFER, vertexCount * 3 * sizeof(float), &vertices[0], GL_STATIC_DRAW);
  
  // create another one for storing our colours
  glGenBuffers(1, &colourBuffer);
  glBindBuffer(GL_ARRAY_BUFFER, colourBuffer);
  glBufferData(GL_ARRAY_BUFFER, vertexCount * 3 * sizeof(float), &colours[0], GL_STATIC_DRAW);
  
  // create a vertex array object encapsulating all our vertex attributes
  glGenVertexArrays(1, &vertexArray);
  glBindVertexArray(vertexArray);
  
  // associate the position array with the vertex array object
  glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
  glVertexAttribPointer(VERTEX_INDEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(VERTEX_INDEX);
  
  // assocaite the colour array with the vertex array object
  glBindBuffer(GL_ARRAY_BUFFER, colourBuffer);
  glVertexAttribPointer(COLOUR_INDEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(COLOUR_INDEX);
  
  // unbind our buffers, resetting to default state
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);
}


void SceneObject::createCube(SceneObject* cube, vec3 pos, float width)
{
  if (!cube)
    cube = new SceneObject();
  
  cube->primativeType = GL_TRIANGLES;
  
  float hw = width / 2.0;
  
  // calculate the cube's vertices
  vec3 a = pos + vec3(-hw, -hw, -hw);
  vec3 b = pos + vec3(hw, -hw, -hw);
  vec3 c = pos + vec3(hw, hw, -hw);
  vec3 d = pos + vec3(-hw, hw, -hw);

  vec3 e = pos + vec3(-hw, hw, hw);
  vec3 f = pos + vec3(hw, hw, hw);
  vec3 g = pos + vec3(hw, -hw, hw);
  vec3 h = pos + vec3(-hw, -hw, hw);
  
  vec3 red = vec3(1.0, 0.0, 0.0);
  //vec3 green = vec3(0.0, 1.0, 0.0);
  vec3 blue = vec3(0.0, 0.0, 1.0);

  // Sides
  cube->pushTriangle(a, b, d, blue, blue, blue);
  cube->pushTriangle(d, b, c, blue, blue, blue);
  
  cube->pushTriangle(b, g, c, red, red, red);
  cube->pushTriangle(c, g, f, red, red, red);

  cube->pushTriangle(g, h, f, blue, blue, blue);
  cube->pushTriangle(f, h, e, blue, blue, blue);

  cube->pushTriangle(h, a, e, red, red, red);
  cube->pushTriangle(e, a, d, red, red, red);

  // Top
  cube->pushTriangle(d, c, e);
  cube->pushTriangle(e, c, f);

  // Bottom
  cube->pushTriangle(b, a, g);
  cube->pushTriangle(g, a, h);
  
  cube->updateVertices();
}


void SceneObject::createCylinder(SceneObject* cylinder, float height, float radius, vec3 pos, mat3 rot)
{
  if (!cylinder)
    cylinder = new SceneObject();
  
  cylinder->primativeType = GL_TRIANGLES;
  
  // number of sections
  int k = 12;
  float u = M_PI * 2.0 / k;

  vec3 top = vec3(0.0, height / 2.0, 0.0);
  vec3 bottom = vec3(0.0, -height / 2.0, 0.0);
  
  vec3 t = rot * top + pos;
  vec3 bo = rot * bottom + pos;
  
  vector<vec3> cs;
  cs.push_back(vec3(1.0,0.0,0.0));
  cs.push_back(vec3(0.0,1.0,0.0));
  cs.push_back(vec3(0.0,0.0,1.0));
  vec3 colour;

  for (int i = 0; i < k; ++i) {
    float theta1 = u * i;
    float theta2 = u * (i+1);
    
    vec3 a = rot * (top + vec3(radius * sin(theta1), 0.0, radius * cos(theta1))) + pos;
    vec3 b = rot * (top + vec3(radius * sin(theta2), 0.0, radius * cos(theta2))) + pos;

    vec3 c = rot * (bottom + vec3(radius * sin(theta1), 0.0, radius * cos(theta1))) + pos;
    vec3 d = rot * (bottom + vec3(radius * sin(theta2), 0.0, radius * cos(theta2))) + pos;
    
    colour = cs[i % cs.size()];
    
    cylinder->pushTriangle(t, a, b, colour, colour, colour);
    cylinder->pushTriangle(a, c, d, colour, colour, colour);
    cylinder->pushTriangle(d, b, a, colour, colour, colour);
    cylinder->pushTriangle(bo, d, c, colour, colour, colour);
  }
  
  cylinder->updateVertices();
}



mat3 setAxisAngleRotationRad(const vec3& a_axis,
                             const float& a_angleRad)
{
  // normalize axis vector
  normalize(a_axis);
  float x = a_axis.x;
  float y = a_axis.y;
  float z = a_axis.z;
  
  // compute rotation matrix
  float c = cos(a_angleRad);
  float s = sin(a_angleRad);
  float v = 1 - c;
  
  mat3 rot;
  
  rot[0][0] = x * x * v + c;      rot[0][1] = x * y * v - z * s;  rot[0][2] = x * z * v + y * s;
  rot[1][0] = x * y * v + z * s;  rot[1][1] = y * y * v + c;      rot[1][2] = y * z * v - x * s;
  rot[2][0] = x * z * v - y * s;  rot[2][1] = y * z * v + x * s;  rot[2][2] = z * z * v + c;
  
  // return success
  return rot;
}











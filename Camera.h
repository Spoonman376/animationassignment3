
#ifndef Camera_h
#define Camera_h

#include <stdio.h>

#include "Utilities.h"

class Camera
{
protected:
  bool needsUpdate = true;
  
  float azu;
  float alt;
  float radius;
  
  float frameOfView;
  float near;
  float far;
  
  vec3 eye = vec3(0, 0, -1);
  vec3 look = vec3(0, 0, 1);
  vec3 up = vec3(0, 1, 0);
  
  float aspectRatio;
  
  vec3 focalPoint;
  
  mat4 viewMatrix;
  mat4 projectionMatrix;
  
  void calculateViewMatrix();
  void calculateProjectionMatrix();
  
  void update();
  
  void calculateSphericalCamera();

public:
  
  Camera();
  
  float getAzumuth();
  void setAzumuth(float newAzu);
  
  float getAltitude();
  void setAltitude(float newAlt);
  
  float getRadius();
  void setRadius(float newRadius);
  
  vec3 getFocalPoint();
  void setFocalPoint(vec3 fp);
  
  void setAspectRatio(float newAsp);
  
  vec3 getEye();
  vec3 getLook();
  vec3 getUp();
  
  void setSphericalCoordinates(float newAzu, float newAlt, float newRadius, vec3 newCenter = vec3(0,0,0));
  
  void lookAt(vec3 eye, vec3 lookAt, vec3 up = vec3(0,1,0));
  
  mat4 getViewMatrix();
  mat4 getProjectionMatrix();
};







#endif /* Camera_h */

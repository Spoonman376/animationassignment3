
#include "Camera.h"
#include <stdio.h>

#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <fstream>
#include <string>
#include <iterator>
#include <assert.h>

#include <glm/gtc/matrix_transform.hpp>



std::string str(glm::vec3 v)
{ return "" + std::to_string(v.x) + ", " + std::to_string(v.y) + ", " + std::to_string(v.z); }


using namespace std;


Camera::Camera()
{
  azu = 0;
  alt = M_PI_4;
  radius = 20;
  
  frameOfView = M_PI / 3.0;
  near = 0.001;
  far = 100;
  
  focalPoint = glm::vec3(0,0,0);
  
  aspectRatio = 1024.0 / 720.0;
  
  calculateProjectionMatrix();
  calculateViewMatrix();
}


void Camera::calculateProjectionMatrix()
{
//  float dy = near * tan(frameOfView * 0.5);
//  float dx = dy * aspectRatio;
//
//  projectionMatrix = glm::mat4(1);
//
//  projectionMatrix[0][0] = near / dx;
//  projectionMatrix[1][1] = near / dy;
//  projectionMatrix[2][2] = -(far + near)/(far - near);
//  projectionMatrix[2][3] = -1;
//  projectionMatrix[3][2] = (2 * far * near) / (far - near);
//  projectionMatrix[3][3] = 1;
  
  projectionMatrix = glm::perspective(frameOfView, aspectRatio, near, far);
}



void Camera::calculateSphericalCamera()
{
  // Calculate view matrix
  float x = radius * sin(alt) * cos(azu);
  float y = radius * cos(alt);
  float z = radius * sin(alt) * sin(azu);
  
  eye = glm::vec3(x,y,z);
  look = -glm::normalize(eye);
  up = glm::vec3(0,1,0);
  
  eye += focalPoint;
  
  calculateViewMatrix();
}



void Camera::lookAt(vec3 e, vec3 l, vec3 u)
{
  eye = e;
  look = normalize(l - e);
  up = normalize(u);
  
  calculateViewMatrix();
  
  focalPoint = l;
  radius = length(l - e);
}



void Camera::calculateViewMatrix()
{
  glm::vec3 n = glm::normalize(look);
  glm::vec3 u = glm::normalize(glm::cross(look, up));
  glm::vec3 v = glm::cross(u,n);
  
  glm::mat4 camera = glm::mat4();
  
  camera[0][0] = u.x;
  camera[1][0] = u.y;
  camera[2][0] = u.z;
  camera[3][0] = 0;
  
  camera[0][1] = v.x;
  camera[1][1] = v.y;
  camera[2][1] = v.z;
  camera[3][1] = 0;
  
  camera[0][2] = -n.x;
  camera[1][2] = -n.y;
  camera[2][2] = -n.z;
  camera[3][2] = 0;
  
  camera[0][3] = 0;
  camera[1][3] = 0;
  camera[2][3] = 0;
  camera[3][3] = 1;
  
  glm::mat4 translation = glm::mat4(1);
  translation[3][0] = -eye.x; // - focalPoint.x;
  translation[3][1] = -eye.y; // - focalPoint.y;
  translation[3][2] = -eye.z; // - focalPoint.z;
  
  viewMatrix = camera * translation;
}



void Camera::setSphericalCoordinates(float newAzu, float newAlt, float newRadius, vec3 newCenter)
{
  azu = newAzu;
  alt = newAlt;
  radius = newRadius;
  focalPoint = newCenter;
  
  calculateSphericalCamera();
}


mat4 Camera::getViewMatrix()
{
  return viewMatrix;
}


mat4 Camera::getProjectionMatrix()
{
  return projectionMatrix;
}


float Camera::getAzumuth()
{
  return azu;
}


void Camera::setAzumuth(float newAzu)
{
  azu = newAzu;
  
  calculateSphericalCamera();
}


float Camera::getAltitude()
{
  return alt;
}

void Camera::setAltitude(float newAlt)
{
  alt = newAlt;
  
  calculateSphericalCamera();
}


float Camera::getRadius()
{
  return radius;
}


void Camera::setRadius(float newRadius)
{
  radius = newRadius;
  
  calculateSphericalCamera();
}


vec3 Camera::getFocalPoint()
{
  return focalPoint;
}

void Camera::setFocalPoint(vec3 fp)
{
  focalPoint = fp;
  
  calculateSphericalCamera();
}


void Camera::setAspectRatio(float newAsp)
{
  aspectRatio = newAsp;
  
  calculateProjectionMatrix();
}


vec3 Camera::getUp()
{
  return up;
}

vec3 Camera::getEye()
{
  return eye;
}

vec3 Camera::getLook()
{
  return look;
}



//
//

#include "Mass.h"

Mass::Mass()
{
  SceneObject::createCube(this);
  setScale(vec3(0.025, 0.025, 0.025));
}

void Mass::updateVelocityAndPosition(float dt)
{
  if (staticObject)
    force = vec3(0.0, 0.0, 0.0);
  
  velocity = velocity + dt * force / mass;
  
  position = position + dt * velocity;
  
  force = vec3(0.0, 0.0, 0.0);
}

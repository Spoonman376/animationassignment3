//
//

#include "GLFW/glfw3.h"

#include "Shader.h"
#include "SceneObject.h"

#include "MassSpringScene.h"

// -------
// Globals
Shader* shader = nullptr;
Camera* camera = nullptr;
MassSpringScene* scene = nullptr;
const int numScenes = 5;
int sceneIndex = 0;

double oldX, oldY;

void printValues();
void printInstructions();

void loadTrackFromFile();

bool fullScreen = false;
void toggleFullScreen(GLFWwindow* window);

void switchScenes();


// --------------------------------------------------------------------------
// OpenGL utility and support function prototypes

void QueryGLVersion();

// --------------------------------------------------------------------------
// GLFW callback functions

// reports GLFW errors
void ErrorCallback(int error, const char* description)
{
  cout << "GLFW ERROR " << error << ":" << endl;
  cout << description << endl;
}

// handles keyboard input events
void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
  if (action != GLFW_PRESS)
    return;

  mat3 rotation(1);

  switch (key) {
    case GLFW_KEY_ESCAPE :
      glfwSetWindowShouldClose(window, GL_TRUE);
      break;
      
    case GLFW_KEY_SPACE :
      break;
      
    case GLFW_KEY_F :
      toggleFullScreen(window);
      break;
      
      
    case GLFW_KEY_LEFT :
      sceneIndex = (sceneIndex - 1) % numScenes;
      switchScenes();
      break;

    case GLFW_KEY_RIGHT :
      sceneIndex = (sceneIndex + 1) % numScenes;
      switchScenes();
      break;


    default :
      break;
  }
}


void toggleFullScreen(GLFWwindow* window)
{
  // toggle state variable
  fullScreen = !fullScreen;
  
  // get handle to monitor
  GLFWmonitor* monitor = glfwGetPrimaryMonitor();
  
  // get information about monitor
  const GLFWvidmode* mode = glfwGetVideoMode(monitor);
  
  // set fullscreen or window mode
  if (fullScreen)
  {
//    glfwSetWindowMonitor(window, monitor, 0, 0, mode->width, mode->height, mode->refreshRate);

    glfwSwapInterval(1);
  }
  else
  {
//    int w = 0.8 * mode->height;
//    int h = 0.5 * mode->height;
//    int x = 0.5 * (mode->width - w);
//    int y = 0.5 * (mode->height - h);
//    glfwSetWindowMonitor(window, NULL, x, y, w, h, mode->refreshRate);
    
    glfwSwapInterval(1);
  }
}

void WindowResizeCallback(GLFWwindow* window, int width, int height)
{
}


void MouseScrollCallback(GLFWwindow* window, double x, double y)
{
  float radius = camera->getRadius();
  
  radius *= (y < 0) ? 1.025 : 0.975;
  
  camera->setRadius(radius);
}




void MouseCursorCallback( GLFWwindow * window, double xpos, double ypos)
{
  // Rotate the camera around the cp
  if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_2))
  {
    float dx, dy;
    dx = xpos - oldX;
    dy = ypos - oldY;
        
    if (!camera)
      return;
    
    if (!glfwGetKey(window, GLFW_KEY_LEFT_SHIFT)) {
      float azu = camera->getAzumuth();
      float alt = camera->getAltitude();
      
      float scale = 0.0025;
      
      camera->setAzumuth(azu + dx * scale);
      camera->setAltitude(alt - dy * scale);
    }
    else {
      vec3 up = camera->getUp();
      vec3 forward = camera->getLook();
      
      vec3 right = normalize(cross(forward, up));

      float scale = 0.002;
      
      vec3 displacement = right * dx * scale - up * dy * scale;
      
      // compute the new position in world coordinates
      vec3 pos = camera->getFocalPoint() + vec3(displacement.x, displacement.y, displacement.z);
      
      camera->setFocalPoint(pos);
    }
  }
  
  oldX = xpos;
  oldY = ypos;
}



void switchScenes()
{
  if (scene != nullptr)
    delete scene;
  
  scene = nullptr;

  switch (sceneIndex) {
    case 1:
      scene = MassSpringScene::createStiffChain(15);
      break;
      
    case 2:
      scene = MassSpringScene::createCloth(30, 20);
      break;

    case 3:
      scene = MassSpringScene::createJellyCube(7);
      break;

    case 4:
      scene = MassSpringScene::createFlag(16, 16);
      break;

    default:
      scene = new MassSpringScene();
      break;
  }
}




// ==========================================================================
// PROGRAM ENTRY POINT

int main(int argc, char *argv[])
{
  // initialize the GLFW windowing system
  if (!glfwInit()) {
      cout << "ERROR: GLFW failed to initilize, TERMINATING" << endl;
      return -1;
  }
  glfwSetErrorCallback(ErrorCallback);

  // attempt to create a window with an OpenGL 4.1 core profile context
  GLFWwindow *window = 0;
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  window = glfwCreateWindow(1024, 720, "Epilipsy Coaster ", 0, 0);
  if (!window) {
      cout << "Program failed to create GLFW window, TERMINATING" << endl;
      glfwTerminate();
      return -1;
  }

  // set keyboard callback function and make our context current (active)
  glfwSetKeyCallback(window, KeyCallback);
  glfwSetCursorPosCallback(window, MouseCursorCallback);
  glfwSetScrollCallback(window, MouseScrollCallback);
  glfwSetWindowSizeCallback(window, WindowResizeCallback);
  
  
  glfwMakeContextCurrent(window);

  // glad stuff (for LINUX)
  #ifdef _LINUX_BUILD
  gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
  #endif

  glClearColor(0.02, 0.02, 0.02, 1.0);

  // call function to load and compile shader programs
  shader = new Shader();
  
  camera = new Camera();
  camera->lookAt(vec3(0,0,4), vec3(0,0,0));
  
  scene = new MassSpringScene();

  printInstructions();
  
  glEnable(GL_DEPTH_TEST);

  // run an event-triggered main loop
  while (!glfwWindowShouldClose(window))
  {
    // clear screen to a dark grey colour
    glClearColor(0.02, 0.02, 0.02, 1.0);
    glClearDepth(MAXFLOAT);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // update camera
    shader->updateProj(camera->getProjectionMatrix());
    shader->updateView(camera->getViewMatrix());
    
    // call function to draw our scene
    scene->render(shader);
    scene->updateScene(1.0 / 60.0);
    
    // scene is rendered to the back buffer, so swap to front for display
    glfwSwapBuffers(window);

    // sleep until next event before drawing again
    glfwPollEvents();
  }
  
  delete scene;

  // clean up allocated resources before exit
  glfwDestroyWindow(window);
  glfwTerminate();

  cout << "Goodbye!" << endl;
  return 0;
}


// --------------------------------------------------------------------------
// Geometry functions



void printValues()
{
  
}

void printInstructions()
{
  cout << endl << endl;
  
  cout << endl << endl;
}


// --------------------------------------------------------------------------
// OpenGL utility functions

void QueryGLVersion()
{
  // query opengl version and renderer information
  string version  = reinterpret_cast<const char *>(glGetString(GL_VERSION));
  string glslver  = reinterpret_cast<const char *>(glGetString(GL_SHADING_LANGUAGE_VERSION));
  string renderer = reinterpret_cast<const char *>(glGetString(GL_RENDERER));

  cout << "OpenGL [ " << version << " ] "
       << "with GLSL [ " << glslver << " ] "
       << "on renderer [ " << renderer << " ]" << endl;
}

// ==========================================================================

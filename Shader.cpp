//
//
#include "Shader.h"

GLuint CompileShader(GLenum shaderType, const string &source);
GLuint LinkProgram(GLuint vertexShader, GLuint fragmentShader);
string LoadSource(const string &filename);



Shader::Shader() {
  // load shader source from files
  string vertexSource = LoadSource("vertex.glsl");
  string fragmentSource = LoadSource("fragment.glsl");
  if (vertexSource.empty() || fragmentSource.empty())
    abort();

  // compile shader source into shader objects
  vertex = CompileShader(GL_VERTEX_SHADER, vertexSource);
  fragment = CompileShader(GL_FRAGMENT_SHADER, fragmentSource);

  // link shader program
  program = LinkProgram(vertex, fragment);

  viewLoc = glGetUniformLocation(program, "view");
  projLoc = glGetUniformLocation(program, "proj");
  modelLoc = glGetUniformLocation(program, "model");

  scaleLoc = glGetUniformLocation(program, "scale");

  // check for OpenGL errors and return false if error occurred
  //!CheckGLErrors();
}


Shader::~Shader()
{
  if (program != 0) {
    glDeleteProgram(program);
  }
  if (vertex != 0) {
    glDeleteShader(vertex);
  }
  if (fragment != 0) {
    glDeleteShader(fragment);
  }
}


void Shader::updateView(glm::mat4 matrix)
{
  glUseProgram(program);
  glUniformMatrix4fv(viewLoc, 1, false, &matrix[0][0]);
  glUseProgram(0);
}

void Shader::updateProj(glm::mat4 matrix)
{
  glUseProgram(program);
  glUniformMatrix4fv(projLoc, 1, false, &matrix[0][0]);
  glUseProgram(0);
}

void Shader::updateModel(glm::mat4 matrix)
{
  glUseProgram(program);
  glUniformMatrix4fv(modelLoc, 1, false, &matrix[0][0]);
  glUseProgram(0);
}


void Shader::updateScale(vec3 scale)
{
  glUseProgram(program);
  glUniform3f(scaleLoc, scale.x, scale.y, scale.z);
  glUseProgram(0);
}





// reads a text file with the given name into a string
string LoadSource(const string &filename)
{
    string source;

    ifstream input(filename);
    if (input) {
        copy(istreambuf_iterator<char>(input),
             istreambuf_iterator<char>(),
             back_inserter(source));
        input.close();
    }
    else {
        cout << "ERROR: Could not load shader source from file "
             << filename << endl;
    }

    return source;
}


// creates and returns a shader object compiled from the given source
GLuint CompileShader(GLenum shaderType, const string &source)
{
    // allocate shader object name
    GLuint shaderObject = glCreateShader(shaderType);

    // try compiling the source as a shader of the given type
    const GLchar *source_ptr = source.c_str();
    glShaderSource(shaderObject, 1, &source_ptr, 0);
    glCompileShader(shaderObject);

    // retrieve compile status
    GLint status;
    glGetShaderiv(shaderObject, GL_COMPILE_STATUS, &status);
    if (status == GL_FALSE)
    {
        GLint length;
        glGetShaderiv(shaderObject, GL_INFO_LOG_LENGTH, &length);
        string info(length, ' ');
        glGetShaderInfoLog(shaderObject, (GLsizei) info.length(), &length, &info[0]);
        cout << "ERROR compiling shader:" << endl << endl;
        cout << source << endl;
        cout << info << endl;
        abort();
    }

    return shaderObject;
}


// creates and returns a program object linked from vertex and fragment shaders
GLuint LinkProgram(GLuint vertexShader, GLuint fragmentShader)
{
    // allocate program object name
    GLuint programObject = glCreateProgram();

    // attach provided shader objects to this program
    if (vertexShader)   glAttachShader(programObject, vertexShader);
    if (fragmentShader) glAttachShader(programObject, fragmentShader);

    // try linking the program with given attachments
    glLinkProgram(programObject);

    // retrieve link status
    GLint status;
    glGetProgramiv(programObject, GL_LINK_STATUS, &status);
    if (status == GL_FALSE)
    {
        GLint length;
        glGetProgramiv(programObject, GL_INFO_LOG_LENGTH, &length);
        string info(length, ' ');
        glGetProgramInfoLog(programObject, (GLsizei) info.length(), &length, &info[0]);
        cout << "ERROR linking shader program:" << endl;
        cout << info << endl;
    }

    return programObject;
}








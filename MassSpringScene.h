//
//

#ifndef MassSpringScene_h
#define MassSpringScene_h

#include <stdio.h>

#include "Mass.h"
#include "Spring.h"

class MassSpringScene : public SceneObject
{
  vector<Mass*> masses;
  vector<Spring*> springs;
  
  float time = 0.0;
  
public:
  
  MassSpringScene();
  
  int numIterations = 5;
  
  vec3 gravity = vec3(0.0, 9.81, 0.0);
  
  bool wind = false;
  vec3 windDirection = vec3(0.0, 0.0, 0.0);
  float windForce = 0.0;
  
  bool groundPlane = false;
  
  float airDamp = 0.1;
  
  void addMass(Mass* mass);
  void addSpring(Spring* spring);
  void clearScene();

  void updateScene(float dt);
  
  static MassSpringScene* createStiffChain(int length);
  static MassSpringScene* createJellyCube(int width);
  static MassSpringScene* createCloth(int width, int height);
  static MassSpringScene* createFlag(int width, int height);
};

#endif /* MassSpringScene_h */

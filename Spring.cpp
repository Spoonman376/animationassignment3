//
//

#include "Spring.h"

Spring::Spring(Mass* A, Mass* B, float k, float d)
{
  a = A;
  b = B;
  
  springConstant = k;
  restLength = d;
  
  criticalDamping = sqrt(k * a->mass);
  
  primativeType = GL_LINES;
  
  updateFromMasses();
}



void Spring::applyForceToMasses()
{
  vec3 force = (a->position - b->position);
  float length = glm::length(force);
  
  normalize(force);
  
  // Get the velocity of the mass projected onto the force
  vec3 velocityA = dot(a->velocity, force) * force;
  vec3 velocityB = dot(b->velocity, force) * force;
  
  force = -springConstant * force * (length - restLength);
  
  float damp = lambda * criticalDamping;
  
  a->force += force - damp * velocityA;
  b->force += -force - damp * velocityB;
}


void Spring::updateFromMasses()
{
  vec3 posA = a->position;
  vec3 posB = b->position;
  
  vertices.clear();
  colours.clear();
  
  vertices.push_back(posA.x);
  vertices.push_back(posA.y);
  vertices.push_back(posA.z);
  
  colours.push_back(1.0);
  colours.push_back(1.0);
  colours.push_back(1.0);
  
  vertices.push_back(posB.x);
  vertices.push_back(posB.y);
  vertices.push_back(posB.z);
  
  colours.push_back(1.0);
  colours.push_back(1.0);
  colours.push_back(1.0);

  updateVertices();
}
